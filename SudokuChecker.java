 
//***********************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Kierra Price and Taylor Black
// CMPSC 111 Spring 2017
// Lab 8
// Date: 3/23/17
//
// Purpose: Create a Sudoku board
//***********************************

import java.util.Scanner;

public class SudokuChecker
{

     Scanner scan = new Scanner(System.in);

     private int w1, w2, w3, w4; //user input w series
     private int x1, x2, x3, x4; //user input x series
     private int y1, y2, y3, y4; //user input y series
     private int z1, z2, z3, z4; //user input z series
     private boolean ro1 = true, ro2 = true, ro3 = true, ro4 = true, co1 = true, co2 = true, co3 = true, co4 = true, re1 = true, re2 = true, re3 = true, re4 = true, Board = true; //to validate Sudoku Board using Boolean methods

     public SudokuChecker() //declares variables until user changes the input
	{
        w1 = 0;
    	w2 = 0;
    	w3 = 0;
    	w4 = 0;
    	x1 = 0;
    	x2 = 0;
    	x3 = 0;
    	x4 = 0;
    	y1 = 0;
    	y2 = 0;
    	y3 = 0;
    	y4 = 0;
    	z1 = 0;
    	z2 = 0;
    	z3 = 0;
    	z4 = 0;
    	ro1 = true;
    	ro2 = true;
    	ro3 = true;
    	ro4 = true;
    	co1 = true;
    	co2 = true;
    	co3 = true;
    	co4 = true;
    	re1 = true;
    	re2 = true;
    	re3 = true;
    	re4 = true;
    	Board = true;
	}

	public void inputGrid()
	{
	
		System.out.println("Enter Row 1:"); //has user enter 4 numbers for row 1
		w1 = scan.nextInt();
		w2 = scan.nextInt();
		w3 = scan.nextInt();
		w4 = scan.nextInt();

		System.out.println("Enter Row 2:"); //has user enter 4 numbers for row 2
		x1 = scan.nextInt();
		x2 = scan.nextInt();
		x3 = scan.nextInt();
	 	x4 = scan.nextInt();

		System.out.println("Enter Row 3:"); //has user enter 4 numbers for row 3
		y1 = scan.nextInt();
		y2 = scan.nextInt();
		y3 = scan.nextInt();
		y4 = scan.nextInt();

		System.out.println("Enter Row 4:"); //has user enter 4 numbers for row 4
		z1 = scan.nextInt();
		z2 = scan.nextInt();
		z3 = scan.nextInt();
    	        z4 = scan.nextInt();

	//validates Regions 1-4
       	System.out.println("System is checking status of region validity..." + "\n");

        if (w1 + w2 + x1 + x2 == 10) {
    		System.out.println("Region 1 input is VALID.");
        		re1 = true;}
            else {
            	System.out.println("Region 1 input is INVALID.");
            	re1 = false;}

            if (w3 + w4 + x3 + x4 == 10) {
        System.out.println("Region 2 input is VALID."); }
            else {
            System.out.println("Region 2 input is INVALID."); }

            if (y1 + y2 + z1 + z2 == 10) {
        System.out.println("Region 3 input is VALID."); }
            else {
            System.out.println("Region 3 input is INVALID."); }

            if (y3 + y4 + z3 + z4 == 10) {
       	System.out.println("Region 4 input is VALID."); }
            else {
            System.out.println("Region 4 input is INVALID."); }

        //validates Row 1-4
       		System.out.println();
    	System.out.println("System is checking status of row validity..." + "\n");
    	if (w1 + w2 + w3 + w4 == 10) {
    	    System.out.println("Row 1 input is VALID.");
    	    ro1 = true;}
        else {
    	    System.out.println("Row 1 input is INVALID.");
            ro1 = false;}
        if (x1 + x2 + x3 + x4 == 10) {
            System.out.println("Row 2 input is VALID.");
            ro2 = true;}
        else {
            System.out.println("Row 2 is INVALID.");
            ro2 = false;}
        if (y1 + y2 + y3 + y4 == 10) {
            System.out.println("Row 3 input is VALID.");
            ro3 = true;}
    	else {
            System.out.println("Row 3 input is INVALID.");
            ro3 = false;}

        if (z1 + z2 + z3 + z4 == 10) {
            System.out.println("Row 4 input is VALID.");
            ro4 = true;}
        else {
            System.out.println("Row 4 input is INVALID.");

            ro4 = false;}

        //validates Column 1-4
		System.out.println();
       	System.out.println("System is checking status of column validity..." + "\n");

       	if (w1 + x1 + y1 + z1 == 10) {
        	System.out.println("Column 1 input is VALID."); }
        else {
            System.out.println("Column 1 input is INVALID."); }

        if (w2 + x2 + y2 + z2 == 10) {
        	System.out.println("Column 2 input is VALID."); }
        else {
            System.out.println("Column 2 input is INVALID."); }

        if (w3 + x3 + y3 + z3 == 10) {
        	System.out.println("Column 3 input is VALID."); }
        else {
            System.out.println("Column 3 input is INVALID."); }

        if (w4 + x4 + y4 + z4 == 10) {
        	System.out.println("Column 4 input is VALID."); }
        else {
            System.out.println("Column 4 input is INVALID."); }

	//validates entire Sudoku Board
        if (ro1 == ro2 == ro3 == ro4 == co1 == co2 == co3 == co4 == re1 == re2 == re3 == re4 == true){
			Board = true;
		}
		else {
			Board = false;
		}

	}

	public void checkGrid() //displays if Sudoku Board is valid or invalid
	{
		System.out.println();
	    System.out.println("System is checking status of Sudoku Board validity..." + "\n");
	    if (Board == true){
			System.out.println("Suduko Board is VALID.");
		}
		else{
			System.out.println("Suduko Board is INVALID.");
		}
	}
}
